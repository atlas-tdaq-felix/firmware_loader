#!/bin/bash

#check that we are running with root priviledges
if [ "$(id -u)" != "0" ]; then  
  echo "WARNING: This script must be run as root or as gitlab-ci"  
fi

if [ $# -ge 2 ]; then
 cardtype=$2
 echo forcing $cardtype as cardtype
 
else
cardtype=`../$BINARY_TAG/flxcard/flx-info | grep "Card type"| awk '{print $4}'`
fi


#determine on which platform we are:
echo we have a $cardtype

if [ $cardtype == "FLX-709" ]; then
   cardpref="FLX709"
   default_bitf="FLX709_GBT_RM0403_4CH_CLKSELECT_GIT_RevertFromHost3_rm-4.3_59_180724_15_15.bit"
   hardware_tag="Digilent/210203A037A5A"
   other_hardware_tag="Digilent/210249A06366"
elif [ $cardtype == "FLX-710" ]; then
#     sudo /tbed/tdaq/felix/scripts/pcie_hotplug_remove.sh
   cardpref="FLX710"
   default_bitf="FLX710_FULLMODE_RM0307_4CH_CLKSELECT_SVN5168_170629_15_02.bit"
   hardware_tag="Xilinx/00001637878f01"
elif [ $cardtype == "FLX-711" ]; then
#     sudo /tbed/tdaq/felix/scripts/pcie_hotplug_remove.sh
   cardpref="FLX711"
   default_bitf="FLX711_RM0304_4CH_CLKSELECT_SVN4500_170308_14_38.bit"
   hardware_tag="Digilent/210249A06366"
elif [ $cardtype == "FLX-712" ]; then
#     sudo /tbed/tdaq/felix/scripts/pcie_hotplug_remove.sh
   cardpref="FLX712"
   default_bitf="FLX711_RM0304_4CH_CLKSELECT_SVN4500_170308_14_38.bit"
   hardware_tag="Digilent/210249A84430"
else
   echo "This card type is unknown. stoping"
   exit 2
fi

fn_i=1;

if [ $# -ge 1 ]; then 
    echo "using given bit file:" $1
    bitfile=$1
else
  echo -n "We have:....................."
  cat /proc/flx  | grep "GIT commit number"
  echo "............................."

  cd /eos/project/f/felix/www/dev/dist/firmware/Bitfiles_development/; 
  bf_list=`ls -1 $cardpref*.bit $cardpref*.tar.gz`;cd - > /dev/null
  for fn in $bf_list; do
     echo $fn_i "] "  $fn
     fn_i=$(expr $fn_i + 1)
  done
  bitfile=""

  echo -n "select the bitfile id from the above list, or just enter for default:"
  echo $default_bitf
  read usersel
  if [ $(expr ${#usersel} ) -eq 0 ]; then
    echo "using the default"
    bitfile=$default_bitf
  else
    fn_i=1;
   for fn in $bf_list; do
     if [ $(expr $usersel) -eq $fn_i ]; then
       echo $fn "selected"
       bitfile=$fn
     fi
   fn_i=$(expr $fn_i + 1)
   done
  fi

# cp  /eos/project/f/felix/www/dev/dist/firmware/Bitfiles_development/${bitfile} bitfiles
# if [  $? -ne 0 ]; then
#  echo "failed to cp the firmware, exiting. Blame EOS. " 
#  exit 2
# fi 

  bitroot=`echo ${bitfile}| cut -f1 -d'.'`
  bitsuffix=`echo ${bitfile}| cut -f2- -d'.'`
  if [ $bitsuffix == "tar.gz" ]; then
    cd bitfiles; tar xzf ${bitfile}; cd - > /dev/null
    bitfile=${bitroot}.bit
  fi

fi

cd /afs/cern.ch/work/f/flx/public/xilinx/Vivado_Lab/latest
source settings64.sh
cd - > /dev/null
echo vivado setup ok

sed -e "s:XXX_filename_XXX:${bitfile}:g"  programFelix_tcl > /tmp/programFelix.1
sed -e "s:HARDWARE_TAG:${hardware_tag}:g"  /tmp/programFelix.1 > /tmp/programFelix.tcl
vivado_lab -mode batch -nolog -nojournal -notrace -source /tmp/programFelix.tcl # 2>&1 >/dev/null

#if [ $cardtype == "FLX-709" ]; then
# otherfile=`ls -tr /eos/project/f/felix/www/dev/dist/firmware/Bitfiles_development/FLX712_GBT_*| tail -1`
# cd /tmp/; tar zxf $otherfile
# obitfile=`ls -tr FLX712_GBT_*.bit|tail -1`
# cd - ; cp /tmp/$obitfile bitfiles/
# sed -e "s:XXX_filename_XXX:${obitfile}:g"  programFelix_tcl > /tmp/programFelix.1
# sed -e "s:HARDWARE_TAG:${other_hardware_tag}:g"  /tmp/programFelix.1 > /tmp/programFelix.tcl
# vivado_lab -mode batch -nolog -nojournal -notrace -source /tmp/programFelix.tcl # 2>&1 >/dev/null
#fi


echo "done, resetting the PCIe bus"
#rm -f /tmp/programFelix.1
#rm -f /tmp/programFelix.tcl
repeatremove=1

while [ $repeatremove -ne 0 ] && [  $repeatremove -lt 5 ]; do
 sudo /tbed/tdaq/felix/scripts/pcie_hotplug_remove.sh
 repeatremove=$?
 echo "repeat remove: $repeatremove"
done
sudo /tbed/tdaq/felix/scripts/pcie_hotplug_rescan.sh

#sudo systemctl stop  tdaq-driver
#sudo systemctl start tdaq-driver

cat /proc/flx | grep "GIT commit number"
echo '~~~~~~~DONE~~~~~~~'
