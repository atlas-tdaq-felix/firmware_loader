#!/bin/bash 

bitfile=$1


echo "xxxxxxxxxxxx" `hostname`
targetdir=/eos/project/f/felix/www/dev/dist/firmware

#cd bitfiles; bitfile=` ls -1tr *.bit|tail -1`; 
#cd - > /dev/null


#check that we are running with root priviledges
if [ "$(id -u)" != "0" ]; then  
  echo "WARNING: This script must be run as root or as gitlab-ci"  
fi

hardware_tag="Digilent/210203A037A5A"
other_hardware_tag="Digilent/210249A06366"


cd /afs/cern.ch/work/f/flx/public/xilinx/Vivado_Lab/latest
source settings64.sh
cd - > /dev/null
echo vivado setup ok
sed -e "s:XXX_filename_XXX:${bitfile}:g"  programFelix_tcl > /tmp/programFelix.1
sed -e "s:HARDWARE_TAG:${hardware_tag}:g"  /tmp/programFelix.1 > /tmp/programFelix.tcl
vivado_lab -mode batch -nolog -nojournal -notrace -source /tmp/programFelix.tcl # 2>&1 >/dev/null
echo $?
echo removing $bitfile
rm -f ./bitfiles/$bitfile
echo '~~~~~~~DONE~~~~~~~ reboot now'
whoami
sudo /sbin/reboot
echo rebooting
