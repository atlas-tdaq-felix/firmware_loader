#!/bin/bash

swvers=$1
echo "This is the nightly AND ci test script for binary:" $BINARY_TAG "using sw release"  ${swvers} assuming $2
echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH

if [ $? -ne 0 ]; then
 echo KERBEROS failure
 exit 1
fi

export BINARY_TAG=${BINARY_TAG}
err_cond=0

# should read automatically
FLXID=2
if [ $2 == "FLX-711" ]; then
FLXID=0
exit 0 
fi 
cd firmware_loader; ./testLast2fwFiles.sh ${swvers} ${FLXID} $2
if [ $? -ne 0 ]; then
 echo "ERROR(s) in testing... "
 err_cond=2
fi
cd ../; ls *.nightly_out

## last word.                                                                                                                                               
 sed -i '$ d' nightly_out.json                                                                                                                           
 echo " } ] }  " >> nightly_out.json   

mkdir /eos/project/f/felix/www/user/latest_nightly/
aDATE=`date +%Y-%m-%d`
cp nightly_out.json /eos/project/f/felix/www/user/latest_nightly/${aDATE}_nightlyOut.json

echo "nightly and ci tests finished..."
if [ $err_cond -ne 0 ]; then
 echo "with ERROR(s)"
 exit 2
fi 
