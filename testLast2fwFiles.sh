#!/bin/bash

flxSWvers=$1
flxID=$2
forceCardID=$3

whoami

export PATH=$PATH:./:../$BINARY_TAG/flxcard/:../$BINARY_TAG/fel/:../$BINARY_TAG/ftools/:../$BINARY_TAG/felix-star/
echo "firmware tester for binary:" $BINARY_TAG " using sw " ${flxSWvers} "   cardID:" $flxID
export LD_LIBRARY_PATH=../$BINARY_TAG/flxcard/:../$BINARY_TAG/regmap/:../$BINARY_TAG/drivers_rcc/:../drivers_rcc/lib64/:$LD_LIBRARY_PATH 
export LD_LIBRARY_PATH=../$BINARY_TAG/fel:../$BINARY_TAG/ftools:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=../$BINARY_TAG/netio-next:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=../external/libfabric/1.10.0.2/x86_64-centos7-gcc8-opt/lib/:$LD_LIBRARY_PATH
echo $LD_LIBRARY_PATH 

curr_vers=`cat /dev/flx | strings  | grep "Reg Map Version" | tail -1| awk '{print $5}'`
echo "Current Reg Map version :" ${curr_vers} 

#--------What we have:
 old_driver_file=`ls -tr /afs/cern.ch/user/j/joos/public/Mark/tdaq_sw_for_Flx-4.*|tail -1`

#--------What we are requested:
if [ $flxSWvers == "master" ]; then
  flx_vers="RM040"
  new_driver_file=`ls -tr /afs/cern.ch/user/j/joos/public/Mark/tdaq_sw_for_Flx-4.*|tail -1`
else
  flx_vers="RM030"
  new_driver_file=`ls -tr /afs/cern.ch/user/j/joos/public/Mark/tdaq_sw_for_Flx-3.*|tail -1`
fi
 
if [ ${#forceCardID} -eq 0 ] ; then
#determine on which platform we are:
 echo `../$BINARY_TAG/flxcard/flx-info `
 cardid=`../$BINARY_TAG/flxcard/flx-info -d $flxID | grep "Card type"| awk '{print $4}'`
 if [ ${#cardid} -eq 0 ] ; then
   echo "Identification problem, assuming 709..."
   cardid="FLX-709"
  fi 
else
   echo "Forcing" $forceCardID
   cardid=$forceCardID
fi
echo we have a $cardid card running sw version ${curr_vers} 
testfilename="tests.dat"

if [ $cardid == "FLX-709" ]; then
   cardpref="FLX709"
   testfilename="709GBTtests.dat"
elif [ $cardid == "FLX-710" ]; then
   cardpref="FLX710"
elif [ $cardid == "FLX-711" ]; then
   cardpref="FLX711"
   testfilename="tests-flx711.dat"
elif [ $cardid == "FLX-712" ]; then
   cardpref="FLX712_"
   testfilename="712GBT-SS-tests.dat"
else
   echo "This card type is unknown. stoping"
   exit 2
fi

echo "Current FW :....................."
cat /proc/flx  | grep -A2 "Build revision"| head -3
cfw_date=`cat /proc/flx|grep -A2 "GIT version" | grep "Date and time" | cut -f2 -d':' | cut -f1 -d'a' |awk -F "-" '{ printf" %02d%02d%02d\n", $3-2000,$2,$1}'`
cfw_svn=`cat /proc/flx|grep "GIT version" | cut -f2 -d':'`
echo "................................."

targetdir=/eos/project/f/felix/www/dev/dist/firmware
#--------------------------------
echo "I am now:" `whoami` 
if [ `hostname|cut -f1 -d'.'` == "${FHOSTID}" ]; then 
 targetdir=/eos/project-f/felix/www/dev/dist/firmware
fi
#cd /eos/project/f/felix/www/dev/dist/firmware/Bitfiles_development/; bf_list=`ls -1tr *${cardpref}*.bit *${cardpref}*.tar.gz | grep ${flx_vers} | tail -2`;cd - > /dev/null
#cd /eos/project/f/felix/www/dev/dist/firmware/Bitfiles_nightly/; nbf_list=`ls -1tr *${cardpref}*.bit *${cardpref}*.tar.gz | grep ${flx_vers} | tail -2`;cd - > /dev/null
cd ${targetdir}/Bitfiles_development/; bf_list=`ls -1tr *${cardpref}*.bit *${cardpref}*.tar.gz | grep -v 48CH | grep -v ITK | tail -2`;cd - > /dev/null
cd ${targetdir}/Bitfiles_nightly/; nbf_list=`ls -1tr *${cardpref}*.bit *${cardpref}*.tar.gz | grep -v 48CH | grep -v ITK  | tail -2`;cd - > /dev/null

echo "LAST TWO devel FW are:" 
echo $bf_list
echo "LAST TWo nigtly FW are:" 
echo $nbf_list

rm -rf bitfiles; mkdir bitfiles

very_first_test=1

if [ -f ../nightly_out.json ]; then
 very_first_test=2;
fi

for fn in $bf_list; do
       echo "WE PREPARE THIS: " $fn 
       cp  ${targetdir}/Bitfiles_development/${fn} bitfiles
       if [  $? -ne 0 ]; then
         echo "failed to cp the firmware, exiting. Blame EOS. " 
         exit 2
       fi
       bitroot=`echo ${fn}| cut -f1 -d'.'`
       bitsuffix=`echo ${fn}| cut -f2- -d'.'`
       if [ $bitsuffix == "tar.gz" ]; then
        cd bitfiles; tar xzf ${bitfile}; cd - > /dev/null
        bitfile=${bitroot}.bit
       fi
done
for fn in $nbf_list; do
       echo "WE PREPARE THIS: " $fn 
       cp  ${targetdir}/Bitfiles_nightly/${fn} bitfiles
       if [  $? -ne 0 ]; then
         echo "failed to cp the firmware, exiting. Blame EOS. " 
         exit 2
       fi
       bitroot=`echo ${fn}| cut -f1 -d'.'`
       bitsuffix=`echo ${fn}| cut -f2- -d'.'`
       if [ $bitsuffix == "tar.gz" ]; then
        cd bitfiles; tar xzf ${bitfile}; cd - > /dev/null
        bitfile=${bitroot}.bit
       fi
done

FHOSTID=pc-t-spare-08

echo "-~~Bitfiles PREP OK~~~- for " $cardid 

#if [ $cardid == "FLX-709" ]; then
 cd bitfiles; for fn in *.tar.gz;do tar zxf $fn; done; bf_list=` ls -1tr *.bit`; cd - > /dev/null
 for fn in $bf_list; do echo $fn; echo and; done;
#------------------------ for both 712 and 709
#if [ $cardid == "FLX-712" ]; then
   echo doing scp now
   tar czf ../myfiles.tgz ../firmware_loader ../flx-firmware-tester ../elinkconfig ../felix-star
   scp ../myfiles.tgz ../felix-nightly-stand-alone-x86_64-centos7-gcc8-opt.tar.gz ${FHOSTID}:
   ssh ${FHOSTID} "tar xzf myfiles.tgz"
   ssh ${FHOSTID} "tar xzf felix-nightly-stand-alone-x86_64-centos7-gcc8-opt.tar.gz"
#fi
#------------------------
 tcount=0;
 for fn in $bf_list; do
  tcount=$(( $tcount + 1 ))
  uploadit=1

  echo now: $fn
  Fgitno=`echo $fn| cut -f2 -d'.'| cut -f2 -d'_'|awk '{print $1}'`
  Cgitno=`cat /proc/flx| grep -m1 'GIT commit number'|cut -f2 -d':'|awk '{print $1}'`

  Ctype=`cat /proc/flx| grep -m1 'Firmware mode'|cut -f2 -d':'`
  Ftype=`echo $fn| cut -f2 -d'_'`
  if [ $Ftype == "FULLMODE" ]; then
   Ftype="FULL"
  fi

  Fdate=`echo $fn| cut -f2 -d'.'| cut -f3 -d'_'`
  Cdate=`cat /proc/flx| grep -m1 'BUILD Date'|cut -f2 -d':'| cut -f 2 -d ' '| awk 'BEGIN { FS="-" }  { printf "%2d%2.2d%2d\n",($3-2000),$2,$1 }'`

  echo "x${Fdate}x vs x${Cdate}x "
  echo "x${Fgitno}x vs x${Cgitno}x"
  echo "x${Ftype}x vs x${Ctype}x"
  if [ $Fgitno -eq $Cgitno ] && [ $Fdate -eq $Cdate ] && [ $Ftype == $Ctype ]; then
   echo Firmware  already in, no upload
   uploadit=0
  fi
 echo "~~test upload: $uploadit" `whoami`

#------------------change firmware
if [ $uploadit -gt 0 ]; then
  echo UPLOADING bitfile.......: $fn
  if [ $cardid == "FLX-712" ]; then
   echo doing ssh now
   ssh ${FHOSTID} "cd firmware_loader; ./programFelix712remote.sh $fn"
   echo sleeping 2 mins
   sleep 120
   adn=`ssh ${FHOSTID} uname -a|wc -w`
   echo $adn
# now rebooting
   until [ $adn -eq 15 ]
    do echo wait a min
       sleep 60
       adn=`ssh ${FHOSTID} uname -a|wc -w`
       echo $adn
    done
   
  else
    programFelix.sh $fn $cardid
  fi
fi

if [[ $fn == "FLX709"*"FULLMODE"* ]]; then
  echo test 709 in FullMode 
  testfilename=709FMtests.dat
fi

if [[ $fn == "FLX709"*"GBT"* ]]; then
  echo test 709 in GBT Mode 
  testfilename=709GBTtests.dat
fi

if [[ $fn == "FLX712_GBT_8CH"* ]]; then
  echo test 712 4ch in GBT Mode 
  testfilename=712GBT8CH-tests.dat
fi

if [[ $fn == "FLX712_GBT_24CH"* ]]; then
  echo test 712 24ch in GBT Mode 
  testfilename=712GBT-SS-tests.dat
fi

if [[ $fn == "FLX712_FULLMODE"* ]]; then
  echo test 712 in Full Mode 
  testfilename=712FMtests.dat
fi
 echo TESTING $fn USING ${testfilename}
 target_hw=`echo $fn | cut -f1 -d'_'` 
 target_fw=`echo $fn | cut -f2- -d'_'| cut -f1 -d'.'`

if [ $cardid == "FLX-712" ]; then
   echo remote test execution
   ssh ${FHOSTID} "source felix-nightly-stand-alone/x86_64-centos7-gcc8-opt/setup.sh; cd flx-firmware-tester; hostname; ./flx-firmware-tester.py -v 2 -f ./${testfilename}  -d ${flxID} -x ${target_hw} -w ${fn} > ../${fn}.nightly_out "
   scp ${FHOSTID}:${fn}.nightly_out .
   scp ${FHOSTID}:flx-firmware-tester/outfile.json .
else
    cp ../flx-firmware-tester/runtwo*.sh .
####-----------------Run tests
    echo  -f ../flx-firmware-tester/${testfilename} -d ${flxID} -x ${target_hw} -w ${fn} 
    ../flx-firmware-tester/flx-firmware-tester.py -v 2 -f ../flx-firmware-tester/${testfilename} -d ${flxID} -x ${target_hw} -w ${fn} > ../${fn}.nightly_out
fi
if [ ${very_first_test} -eq 1 ]; then
     very_first_test=2
else
      sed -i '1d' outfile.json
fi
cat outfile.json >> ../nightly_out.json


 echo NEXT...
done

echo Tests Done... 

if [ 1 -eq 0 ]; then
  rm -f bitfiles/*
  bitfile=$currentfw
  cp  ${targetdir}/Bitfiles_development/${bitfile} bitfiles
  if [  $? -ne 0 ]; then
         echo "failed to cp the firmware, exiting. Blame EOS. " 
         exit 2
  fi

  cd bitfiles; for fn in *.tar.gz;do tar zxf $fn; done; 
  bf_list=` ls -1tr *.bit`; cd - > /dev/null
  for fn in $bf_list; do
     echo Uploading $fn
     programFelix.sh $fn $cardid
  done
fi
  echo ALL FINISHED.

